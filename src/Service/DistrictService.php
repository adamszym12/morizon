<?php

namespace App\Service;

use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class DistrictService
{
    /**
     * @param  HttpClientInterface  $client
     */
    public function __construct(private readonly HttpClientInterface $client)
    {
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function fetchData(string $url, string $start, string $end): array
    {
        $data = [];

        for ($i = $start; $i <= $end; $i++) {
            $response = $this->client->request(
                'GET',
                "$url=$i"
            );

            $content = strip_tags($response->getContent());

            $data[] = $content;
        }

        return $data;
    }

    /**
     * @param  array  $data
     * @return array
     */
    public function parseGdansk(array $data): array
    {
        $parsedData = [];
        $pattern = "/(.+)Powierzchnia: ([0-9.,]+) km2Liczba ludności: (\d+) osób/";

        foreach ($data as $value) {
            preg_match($pattern, $value, $matches);

            $name = trim($matches[1]);
            $area = trim($matches[2]);
            $population = trim($matches[3]);

            $parsedData[] = [
                'name'       => $name,
                'area'       => str_replace(',', '.', $area) * 1000000,
                'population' => $population,
            ];
        }

        return $parsedData;
    }

    /**
     * @param  array  $data
     * @return array
     */
    public function parseKrakow(array $data): array
    {
        $parsedData = [];

        $pattern = "/Dzielnica [A-Za-z]+ (.+) \(.*\)-.*Powierzchnia dzielnicy: ([0-9.,]+) ha.*Liczba mieszkańców \(.*\): ([0-9]+)/s";
        foreach ($data as $value) {
            preg_match($pattern, $value, $matches);

            $name = trim($matches[1]);
            $area = trim($matches[2]);
            $population = trim($matches[3]);

            $parsedData[] = [
                'name'       => $name,
                'area'       => str_replace(',', '.', $area) * 10000,
                'population' => $population,
            ];
        }

        return $parsedData;
    }
}
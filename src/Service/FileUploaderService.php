<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploaderService
{
    private string $uploadDirectory;

    /**
     * @param  string  $uploadDirectory
     */
    public function __construct(string $uploadDirectory)
    {
        $this->uploadDirectory = $uploadDirectory;
    }

    /**
     * @param  UploadedFile  $file
     * @param  string  $name
     * @param  string|null  $imagePath
     * @return void
     */
    public function upload(UploadedFile $file, string $name, ?string $imagePath): void
    {
        if ($imagePath && $imagePath != '/uploads/default_image.jpg') {
            unlink(str_replace('/uploads', '', $this->uploadDirectory).$imagePath);
        }

        $file->move($this->uploadDirectory, $name);
    }
}
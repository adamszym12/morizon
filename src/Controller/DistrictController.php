<?php

namespace App\Controller;

use App\Entity\District;
use App\Repository\DistrictRepository;
use App\Service\DistrictService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DistrictController extends AbstractController
{
    /**
     * @param  DistrictService  $districtService
     * @param  DistrictRepository  $districtRepository
     */
    public function __construct(
        private readonly DistrictService $districtService,
        private readonly DistrictRepository $districtRepository,
    ) {
    }

    /**
     * @return Response
     */
    #[Route('/district', name: 'app_district')]
    public function index(): Response
    {
        return $this->render('district/index.html.twig');
    }

    /**
     * @param  EntityManagerInterface  $entityManager
     * @return RedirectResponse
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    #[Route('/district/fetch', name: 'app_district_fetch')]
    public function fetch(EntityManagerInterface $entityManager): RedirectResponse
    {
        $data = $this->districtService->fetchData("https://www.gdansk.pl/subpages/dzielnice/html/4-dzielnice_mapa_alert.php?id",
            1, 35);

        $parsedData = $this->districtService->parseGdansk($data);

        $data = $this->districtService->fetchData("https://www.bip.krakow.pl/?bip_id=1&mmi",
            25527, 25544);

        $parsedData = array_merge($this->districtService->parseKrakow($data), $parsedData);

        foreach ($parsedData as $data) {
            if(!$district = $this->districtRepository->findOneBy(['name' => $data['name']])){
                $district = new District();
            }

            $district->setArea($data['area']);
            $district->setName($data['name']);
            $district->setPopulation($data['population']);

            $entityManager->persist($district);
        }

        $entityManager->flush();

        return $this->redirectToRoute('app_district');
    }
}

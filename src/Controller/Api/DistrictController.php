<?php

namespace App\Controller\Api;

use App\Repository\DistrictRepository;
use App\Service\FileUploaderService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Uid\Uuid;

class DistrictController extends AbstractController
{
    /**
     * @param  FileUploaderService  $fileUploaderService
     * @param  DistrictRepository  $districtRepository
     * @param  EntityManagerInterface  $entityManager
     * @param  SerializerInterface  $serializer
     */
    public function __construct(
        private readonly FileUploaderService $fileUploaderService,
        private readonly DistrictRepository $districtRepository,
        private readonly EntityManagerInterface $entityManager,
        private readonly SerializerInterface $serializer,
    ) {
    }

    /**
     * @param  Request  $request
     * @return Response
     */
    #[Route('/api/district', name: 'app_api_district')]
    public function index(Request $request): Response
    {
        $districts = $this->districtRepository->findFilteredAndSorted($request->query);
        $jsonDistricts = $this->serializer->serialize($districts, 'json');

        return new Response($jsonDistricts);
    }

    /**
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    #[Route('/api/district/{id}', name: 'upload_image', methods: ['POST'])]
    public function update(Request $request, int $id): Response
    {
        $uploadedFile = $request->files->get('image');

        if (!$uploadedFile) {
            return new JsonResponse('Brak zdjęcia.', Response::HTTP_BAD_REQUEST);
        }

        $district = $this->districtRepository->find($id);
        $name = Uuid::v4();

        $this->fileUploaderService->upload($uploadedFile, $name, $district->getImagePath());

        $district->setImagePath("/uploads/$name");

        $this->entityManager->persist($district);
        $this->entityManager->flush();

        return new JsonResponse('Zdjęcie zakutalizowane pomyślnie.', Response::HTTP_OK);
    }
}

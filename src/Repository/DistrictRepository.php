<?php

namespace App\Repository;

use App\Entity\District;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\InputBag;

/**
 * @extends ServiceEntityRepository<District>
 *
 * @method District|null find($id, $lockMode = null, $lockVersion = null)
 * @method District|null findOneBy(array $criteria, array $orderBy = null)
 * @method District[]    findAll()
 * @method District[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DistrictRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, District::class);
    }

    public function findFilteredAndSorted(InputBag $query)
    {
        $sortField = $query->get('sort', 'id');
        $sortOrder = $query->get('order', 'asc');
        $nameFilter = $query->get('name');
        $areaMinFilter = $query->get('areaMin');
        $areaMaxFilter = $query->get('areaMax');
        $populationMinFilter = $query->get('populationMin');
        $populationMaxFilter = $query->get('populationMax');

        $queryBuilder = $this->createQueryBuilder('d');

        if ($nameFilter) {
            $queryBuilder->andWhere('d.name LIKE :name')->setParameter('name', "%$nameFilter%");
        }

        if ($areaMinFilter) {
            $queryBuilder->andWhere('d.area >= :areaMin')->setParameter('areaMin', $areaMinFilter);
        }

        if ($areaMaxFilter) {
            $queryBuilder->andWhere('d.area <= :areaMax')->setParameter('areaMax', $areaMaxFilter);
        }

        if ($populationMinFilter) {
            $queryBuilder->andWhere('d.population >= :populationMin')->setParameter('populationMin',
                $populationMinFilter);
        }

        if ($populationMaxFilter) {
            $queryBuilder->andWhere('d.population <= :populationMax')->setParameter('populationMax',
                $populationMaxFilter);
        }

        $queryBuilder->orderBy("d.$sortField", $sortOrder);

        return $queryBuilder->getQuery()->getResult();
    }
}

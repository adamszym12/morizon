/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';

// start the Stimulus application
import './bootstrap';

import 'bootstrap/dist/js/bootstrap'
import 'bootstrap/dist/css/bootstrap.css'


import { createApp } from 'vue';
import DistrictIndexComponent from "./components/DistrictIndexComponent.vue";
import Notifications from '@kyvg/vue3-notification'


const app = createApp({});
app.component('DistrictIndexComponent', DistrictIndexComponent);
app.use(Notifications)
app.mount('#app');
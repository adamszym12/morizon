## Uruchomienie aplikacji
Aplikacja posiada konfigurację obrazów dockerowych
```
# zbudowanie obrazu i uruchomienie kontenera aplikacji
docker-compose up -d

# migracja
docker-compose exec php-dev php bin/console doctrine:migrations:migrate

# Vue
docker-compose run npm-dev npm install 
docker-compose run npm-dev npm run build 


# Ja osobiście korzystam z traefika, stąd te dziwne parametry w docker-compose.
W innym przypadku naleźy wejść w aplikację przez ip kontenera

docker inspect morizon-php | grep '"IPAddress"' | tail -n 1

Rozwiązanie znajduje się pod adresem /district
```

```
# Mankamenty z których zdaję sobię sprawę
- brak lazyloadingu
- brak paginacji
- brak walidacji
- brak responsywności
- brak obróbki zdjęć (crop i inne)
- pobieranie pewnie powinno być zakolejkowane bo trwa długo
- filtry mogą się wykluczać
```